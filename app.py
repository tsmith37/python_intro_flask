import os
import shutil
from flask import Flask,render_template, request,json
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
UPLOAD_FOLDER = '/templates/uploads/'

app = Flask(__name__)

app.debug = True
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def hello():
    return 'Welcome to Python Flask!'

@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/help')
def help():
    return render_template('example.html')


@app.route('/uploadData', methods=['GET', 'POST', 'PUT'])
def uploadData():

    if request.method == 'PUT':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(file.filename)

            os.system('python3 contoursFunction.py '+file.filename)
            os.remove(file.filename)

            source = 'output.jpg'
            dest = 'static/output.jpg'
            os.rename(source, dest)
            return render_template('uploadImage.html')

if __name__=="__main__":
    app.run(debug=True,host='0.0.0.0')
