$(function(){
	$('button').click(function(){
		
		var output = document.getElementById('file'),
		file = output.files[0]

		var data = new FormData();
			data.append("file", document.getElementById('file').files[0])

	        var config = {
    	         'Content-Type': data.type
                };

                axios.put('/uploadData', data, config)
                 .then(function (res) {
                  output.className = 'container';
                  output.innerHTML = res.data;
                 })
                 .catch(function (err) {
                  output.className = 'container text-danger';
                  output.innerHTML = err.message;
                 });


	});
});
